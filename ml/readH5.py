import h5py
filename = "/home/fwinkel/Desktop/H_aa_qqqq/trees/user.scheong.35418033._000001.output.h5"

with h5py.File(filename, "r") as f:
    
    # Print all root level object names (aka keys)
    keys = f.keys()
    print("Keys in file: %s" % keys)
    
    # Must turn 'KeysViewHDF5' object into list because is not subscriptable  
    keys = list(keys)

    print("==== Looping on keys ====")
    for k in keys: 
        dataset = f[k]
        fields = dataset.dtype.fields
        
        print(f"Key: {k}")
        print(f"  Info: * {dataset}")
        print(f"        * Number of fields = {len(fields)}")
        for field in fields:
            print(f"           * {field}") 
