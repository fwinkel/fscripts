#########################################################################################
# Code used for jet classification. Spent a lot of time on it, is worth to have it here #
# Takes rootfile with NN inputs, builds Keras model, trains and test it                 #
#########################################################################################

import os
import sys
import uproot
import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib as mpl
import matplotlib.pyplot as plt
from tqdm import tqdm
from tensorflow.keras import initializers
from tensorflow.keras.optimizers import SGD
from keras.models import Sequential, Model
from keras.models import load_model
from keras.layers import Input, Dense
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
#from sklearn import preprocessing
from sklearn.preprocessing import MinMaxScaler, Normalizer
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import roc_curve
from sklearn.metrics import roc_auc_score
from sklearn.model_selection import train_test_split
from sklearn.model_selection import GridSearchCV
from pandas_tfrecords import pd2tf, tf2pd
from datetime import datetime

# -------------------------------------------- Random seeds --------------------------------------------------------------
from numpy.random import seed
seed(1)
tf.random.set_seed(2)
# ------------------------------------------------------------------------------------------------------------------------

pd.options.mode.chained_assignment = None

class jetClassifier:
    # initilize class
    def __init__(self):
        #### NN's parameters #########
        self.nNodes             = 100         
        self.nEpochs            = 50
        self.batchSize          = 100 
        self.learningRate       = 1e-3
        self.loadTrainedModel   = True
        #### Code's parameters #######
        self.printListOfKeys    = False
        self.trainWithWeights   = True  
        self.weightedHistograms = True 
        self.weightedROCs       = True 
        self.min_pT             = 30 
        self.max_pT             = 50
        self.plotInputs         = False
        self.trainWithTFRecord  = True
        self.writeTFRecord      = False
        self.addGammaROC        = True
        self.inputMoments       = ["JetPt", 
                                   "JetY", 
                                   "JetWidth",
                                   "LatContRad_5", 
                                   "LatContRad_75", 
                                   "LatContRad_9", 
                                   "NumTowers", 
                                   "NumTowers_t", 
                                   "TowTimeMean",
                                   "TowTimeRMS"#,
                                   #"Gamma"
                                  ]

    def treesToDataFrames(self, inFile, results_dir):
        # open root file with uproot
        print("---------------------------------------------")
        print("     Opening files with uproot               ")
        print("---------------------------------------------")
        f = uproot.open(inFile)
        
        #print variables that are being used
        print("Input moments:")
        for i in self.inputMoments:
            print("   -"+i)
        
        ## print tree names
        if self.printListOfKeys == True:
            print("Printing list of keys")
            for i in f.keys():
                print("--KEY: "+i)
        
        # ========================================================================================================================
        # =============================== Convert ntuple into Pandas DataFrame ===================================================
        # ========================================================================================================================
        
        df_HS    = f['treeHS'].arrays(library = "pd")
        df_QCDPU = f['treeQCD'].arrays(library = "pd")
        df_Sto   = f['treeSto'].arrays(library = "pd")

        df_QCD = pd.concat([df_QCDPU, df_HS])
        df_QCD = df_QCD.sample(frac=1).reset_index(drop=True)

        # print number of events in tree
        print("-------------Before cuts--------")
        print(f"Number of QCD jets = {df_QCD.shape[0]}")
        print(f"Number of Sto jets = {df_Sto.shape[0]}")
        
        #------------------------------------ Cuts on full sample -----------------------------------------
        df_QCD = df_QCD[df_QCD['JetPt'] >= self.min_pT]
        df_Sto = df_Sto[df_Sto['JetPt'] >= self.min_pT]
        df_QCD = df_QCD[df_QCD['JetPt'] <= self.max_pT]
        df_Sto = df_Sto[df_Sto['JetPt'] <= self.max_pT]

        df_QCD = df_QCD[df_QCD['JetWidth'] <= 0.4]
        df_Sto = df_Sto[df_Sto['JetWidth'] <= 0.4]

        #df_QCD = df_QCD[df_QCD['Gamma'] >= 0]
        #df_Sto = df_Sto[df_Sto['Gamma'] >= 0]

        print("-------------After cuts---------")
        print(f"Number of QCD jets = {df_QCD.shape[0]}")
        print(f"Number of Sto jets = {df_Sto.shape[0]}")
        
        #----------------------------------- Preprocessing ------------------------------------------------
        # insert a column for tagging (1 for QCD jets, 0 for Sto jets)
        df_QCD.insert(df_QCD.shape[1], "tag", np.ones(df_QCD.shape[0]))
        df_Sto.insert(df_Sto.shape[1], "tag", np.zeros(df_Sto.shape[0]))
        
        # divide weights by their mean to reduce impact in loss function
        #df_QCD["weight"] = df_QCD["weight"]/df_QCD["weight"].mean()
        #df_Sto["weight"] = df_Sto["weight"]/df_Sto["weight"].mean()
        
        df = pd.concat([df_QCD, df_Sto])
        df = df.sample(frac = 1).reset_index(drop=True)

        # drop rows with nans
        df = self.cleanDataFrames(df, self.inputMoments)

        for j in self.inputMoments: 
            #df[j] = df[j] /df[j].abs().max()
            if j == "JetPt"        : df[j] = self.scaling(df[j], self.max_pT) 
            if j == "JetY"         : df[j] = self.scaling(df[j], 4.5)
            if j == "JetWidth"     : df[j] = self.scaling(df[j], 0.4)
            if j == "LatContRad_5" : df[j] = self.scaling(df[j], 0.4)
            if j == "LatContRad_75": df[j] = self.scaling(df[j], 0.4)
            if j == "LatContRad_9" : df[j] = self.scaling(df[j], 0.4)
            if j == "NumTowers"    : df[j] = self.scaling(df[j], 100)
            if j == "NumTowers_t"  : df[j] = self.scaling(df[j], 100)
            if j == "TowTimeMean"  : df[j] = self.scaling(df[j], 30)
            if j == "TowTimeRMS"   : df[j] = self.scaling(df[j], 30)
            if j == "Gamma"        : df[j] = self.scaling(df[j], 10)

        X_train, X_test, y_train, y_test = train_test_split( df[self.inputMoments + ['tag'] + ['weight']] , df['tag'], test_size=0.3, random_state=1)

        # taking a look at the inputs for training 
        if self.plotInputs == True:
            self.inputsPlotter(X_train.loc[X_train['tag']==1], X_train.loc[X_train['tag']==0] , results_dir)
         
        print("-------- All jets considered ---------------")
        print(f"Training QCD set lenght is {len(X_train.loc[X_train['tag']==1])}")
        print(f"Training Sto set lenght is {len(X_train.loc[X_train['tag']==0])}")
        print(f"Testing  QCD set lenght is {len(X_test.loc[X_test['tag']==1])}")
        print(f"Testing  Sto set lenght is {len(X_test.loc[X_test['tag']==0])}")
        print("--------------------------------")
        
        return X_train, X_test, y_train, y_test

    # function for scaling inputs
    def scaling(self, df, a):
        df = df/a
        df.loc[df> 1] =  1 
        df.loc[df<-1] = -1 
        return df

    # function for dropping NaNs (if any) in the dataframes
    def cleanDataFrames(self, df, inputList):
        for i in inputList:
            check_for_nan = df[i].isnull().values.any()
            count_nan     = df[i].isnull().sum()
            if check_for_nan == True:
                print("-------------------------------------------------------------------------------------------------------------------")
                print("Warning: Variable "+i+" has "+str(count_nan)+" NaN values, their rows will be removed in order to keep clean inputs")
                print("-------------------------------------------------------------------------------------------------------------------")
                df.dropna(subset = [i], inplace = True)
        return df       

    # function for plotting inputs for training
    def inputsPlotter(self, df_train_QCD, df_train_Sto, resultsDir):
        print("---------------------------------------------")
        print("     Plotting inputs                         ")
        print("---------------------------------------------")
        
        inputsControl_QCD  = df_train_QCD.dropna()
        weights_QCD        = inputsControl_QCD[["weight"]].to_numpy()
        inputsControl_Sto  = df_train_Sto.dropna()
        weights_Sto        = inputsControl_Sto[["weight"]].to_numpy()
        for j,i in enumerate(self.inputMoments):
            hs = inputsControl_QCD[[i]].to_numpy()
            pu = inputsControl_Sto[[i]].to_numpy()
            plt.hist(hs, bins = 100, density = True, weights = weights_QCD, color = 'cornflowerblue', alpha = 1,   label = "QCD jets")
            #plt.hist(hs, bins = 100, density = True, color = 'cornflowerblue', alpha = 1,   label = "QCD jets")
            plt.hist(pu, bins = 100, density = True, weights = weights_Sto, color = 'darkmagenta',    alpha = 0.4, label = "Sto jets")
            #plt.hist(pu, bins = 100, density = True, color = 'darkmagenta',    alpha = 0.4, label = "Sto jets")
            plt.title("Input variables for training")
            plt.legend(loc = 'best')
            plt.xlabel(i)
            plt.ylabel("Entries (Normalized)")
            #if i=="JetPt": plt.yscale("log")
            #if i=="JetWidth": 
            #    plt.xlim([0,1])
            #if i=="TowTimeMean": plt.xlim([-30,30])    
            #else: 
            #    plt.xlim([min(inputsControl_QCD[[i]].to_numpy()), max(inputsControl_QCD[[i]].to_numpy())])
            #plt.grid()
            plt.savefig(resultsDir+f"inputVar_{i}")
            plt.show()

    # define the keras model
    def buildModel(self, unit1):
        print("---------------------------------------------")
        print("     Building model                          ")
        print("---------------------------------------------")

        ## the model 
        Input_1 = Input(shape = (len(self.inputMoments), ),    name = "Input"  )
        x       = Dense(units = unit1, activation = 'tanh',    name = 'Layer1' )(Input_1)
        x       = Dense(units = unit1, activation = 'tanh',    name = 'Layer2' )(x)
        #x       = Dense(units = unit1, activation = 'tanh',    name = 'Layer3' )(x)
        #x       = Dense(units = unit1, activation = 'tanh',    name = 'Layer4' )(x)
        out     = Dense(1,             activation = 'sigmoid', name = 'Output',)(x)

        # put everything together and compile
        model   = Model(inputs = Input_1, outputs = out)
        model.compile(loss = 'binary_crossentropy', optimizer = 'adam', metrics = ['accuracy'])
        return model

    def unweightedScoringHistos(self, s, b, s_train, b_train, bins):
        print("---------------------------------------------")
        print("     Making unweighted scoring histograms    ")
        print("---------------------------------------------")
       
        hist_s, binEdges_s = np.histogram(s, density = True, bins = bins)
        hist_b, binEdges_b = np.histogram(b, density = True, bins = bins)
    
        hist_s_train, binEdges_s_train = np.histogram(s_train, density = True, bins = bins)
        hist_b_train, binEdges_b_train = np.histogram(b_train, density = True, bins = bins)
        return hist_s, hist_b, binEdges_s, binEdges_b, hist_s_train, hist_b_train, binEdges_s_train, binEdges_b_train

    def WeightedScoringHistos(self, s, b, s_train, b_train, bins, s_weights, b_weights, s_train_weights, b_train_weights):
        print("---------------------------------------------")
        print("     Making weighted scoring histograms      ")
        print("---------------------------------------------")

        hist_s, binEdges_s = np.histogram(s, density = True, bins = bins, weights = s_weights)
        hist_b, binEdges_b = np.histogram(b, density = True, bins = bins, weights = b_weights)

        hist_s_train, binEdges_s_train = np.histogram(s_train, density = True, bins = bins, weights = s_train_weights)
        hist_b_train, binEdges_b_train = np.histogram(b_train, density = True, bins = bins, weights = b_train_weights)
        return hist_s, hist_b, binEdges_s, binEdges_b, hist_s_train, hist_b_train, binEdges_s_train, binEdges_b_train

    def unweightedRocCurves(self, y_train, y_test, s_predictions_train, s_predictions_test):
        print("---------------------------------------------")
        print("     Making unweighted ROC curves            ")
        print("---------------------------------------------")

        fpr_train, tpr_train, thresholds_train = roc_curve(y_train, s_predictions_train)
        fpr_test, tpr_test, thresholds_test    = roc_curve(y_test, s_predictions_test  )
        
        trainAUC = roc_auc_score(y_train, s_predictions_train)
        trainAUC = round(trainAUC,3)
        testAUC  = roc_auc_score(y_test, s_predictions_test)
        testAUC  = round(testAUC,3)
        return fpr_train, fpr_test, tpr_train, tpr_test, trainAUC, testAUC, thresholds_train, thresholds_test

    def WeightedRocCurves(self, y_train, y_test, s_predictions_train, s_predictions_test, w_train, w_test):
        print("---------------------------------------------")
        print("     Making weighted ROC curves              ")
        print("---------------------------------------------")

        fpr_train, tpr_train, thresholds_train = roc_curve(y_train, s_predictions_train, sample_weight = w_train)
        fpr_test, tpr_test, thresholds_test    = roc_curve(y_test, s_predictions_test  , sample_weight = w_test)
        
        trainAUC = roc_auc_score(y_train, s_predictions_train, sample_weight = w_train)
        trainAUC = round(trainAUC,3)
        testAUC  = roc_auc_score(y_test, s_predictions_test, sample_weight = w_test)
        testAUC  = round(testAUC,3)
        return fpr_train, fpr_test, tpr_train, tpr_test, trainAUC, testAUC, thresholds_train, thresholds_test

    def PerformancePlots(self, results_dir, subdirName, history, nLayers, 
                                                                 y_train, 
                                                                 w_train, 
                                                                 predictions_train, 
                                                                 y_test, 
                                                                 w_test, 
                                                                 predictions_test):
        print("---------------------------------------------")
        print("     Performance plots                       ")
        print("---------------------------------------------")

        subdirName = subdirName
        newDir     = os.path.join(results_dir, subdirName)
        if not os.path.isdir(newDir):
            os.makedirs(newDir)
    
        #labels
        label = ''
        if (subdirName=='0/'): label = '20<pT[GeV]<150' 
        if (subdirName=='2/'): label = '20<pT[GeV]<30' 
        if (subdirName=='4/'): label = '30<pT[GeV]<50' 
        if (subdirName=='6/'): label = '50<pT[GeV]<150'
    
        if (subdirName=='output/'): label = f'{self.min_pT}<pT[GeV]<{self.max_pT}' 

    
        print('------------ '+label+' --------------')

        if self.loadTrainedModel == False :
            #if (subdirName=='0/') or (subdirName=='output/'):
            ##if (subdirName=='zzz/'):
            #    # plot accuracy vs epochs
            #    print("---------------------------------------------")
            #    print("     Plotting accuracy vs epochs             ")
            #    print("---------------------------------------------")
            #    plt.plot(history.history['accuracy'], label = 'training set accuracy')
            #    plt.plot(history.history['val_accuracy'], label = 'validation set accuracy')
            #    plt.plot(history.history['loss'], label = 'training set loss')
            #    plt.plot(history.history['val_loss'], label = 'validation set loss')
            #    plt.title('accuracy and loss vs epochs')
            #    plt.ylabel('accuracy/loss')
            #    plt.xlabel('epoch')
            #    plt.legend(loc='best')
            #    plt.xlim([0, self.nEpochs])
            #    plt.grid()
            #    plt.savefig(newDir+"accuracy_vs_epochs.pdf")
            #    #plt.show()
                
            # plot loss function vs epochs
            print("---------------------------------------------")
            print("     Plotting loss vs epochs                 ")
            print("---------------------------------------------")
            plt.plot(history.history['loss'], label = 'training set')
            plt.plot(history.history['val_loss'], label = 'validation set')
            plt.title('model loss')
            plt.ylabel('loss')
            plt.xlabel('epoch')
            plt.legend(loc='best')
            plt.xlim([0, self.nEpochs])
            plt.grid()
            plt.savefig(newDir+"loss_vs_epochs.pdf")
            plt.show()
     
        # delete unused variables
        #if (self.weightedHistograms==False): del w_train, w_test    
        
        # plot NNs output
        s_train = []
        s_train_weights = []
        b_train = []
        b_train_weights = []


        iterator = tqdm(range(len(predictions_train)), desc='Histogramming training set scoring')
        for i in iterator:
            if (y_train.iloc[i]==0):
                b_train.append(predictions_train[i][0])
                if (self.weightedHistograms==True):
                    b_train_weights.append(w_train.iloc[i])
            if (y_train.iloc[i]==1):
                s_train.append(predictions_train[i][0])
                if (self.weightedHistograms==True):
                    s_train_weights.append(w_train.iloc[i])
        
        s = []
        s_weights = []
        b = []
        b_weights = []

        iterator = tqdm(range(len(predictions_test)), desc='Histogramming testing set scoring')
        for i in iterator:
            if (y_test.iloc[i]==0):
                b.append(predictions_test[i][0])
                if (self.weightedHistograms==True):
                    b_weights.append(w_test.iloc[i])
            if (y_test.iloc[i]==1):
                s.append(predictions_test[i][0])
                if (self.weightedHistograms==True):
                    s_weights.append(w_test.iloc[i])
    
        nbins    = 61
        bins = np.linspace(0, 1, nbins)
        binWidth = bins[1]-bins[0]

        if (self.weightedHistograms == True):
            hist_s, hist_b, binEdges_s, binEdges_b, hist_s_train, hist_b_train, binEdges_s_train, binEdges_b_train = self.WeightedScoringHistos(s, b, 
                                                                                                                                                s_train, b_train, 
                                                                                                                                                bins, 
                                                                                                                                                s_weights,       b_weights, 
                                                                                                                                                s_train_weights, b_train_weights)
        elif (self.weightedHistograms == False):
            hist_s, hist_b, binEdges_s, binEdges_b, hist_s_train, hist_b_train, binEdges_s_train, binEdges_b_train = self.unweightedScoringHistos(s, b, 
                                                                                                                                                  s_train, b_train, 
                                                                                                                                                  bins)
        err_s = np.sqrt(hist_s/(len(s)*binWidth))
        err_b = np.sqrt(hist_b/(len(b)*binWidth))
        err_s_train = np.sqrt(hist_s_train/(len(s_train)*binWidth))
        err_b_train = np.sqrt(hist_b_train/(len(b_train)*binWidth))
    
        
        fig, ax = plt.subplots(2)
        plt.subplots_adjust(hspace = .001)
        
        ax[0].bar(bins[:-1]+(bins[1]-bins[0])/2, hist_s, width=binWidth, align='center', 
                  color='cornflowerblue', capsize=2, alpha=1,   label='signal',     yerr=err_s, ecolor='cornflowerblue')
        ax[0].bar(bins[:-1]+(bins[1]-bins[0])/2, hist_b, width=binWidth, align='center', 
                  color='darkmagenta',    capsize=2, alpha=0.7, label='background', yerr=err_b, ecolor='darkmagenta')
        ax[0].bar(bins[:-1]+(bins[1]-bins[0])/2, hist_s_train, width=binWidth, align='center', 
                  color='none', edgecolor='blue',capsize=2, alpha=1, label='signal (train)',    yerr=err_s_train, ecolor='blue')
        ax[0].bar(bins[:-1]+(bins[1]-bins[0])/2, hist_b_train, width=binWidth, align='center', 
                  color='none', edgecolor='red', capsize=2, alpha=1, label='background (train)',yerr=err_b_train, ecolor='red')


        ax[0].set_title(f"NN's output ( {label} )")
        ax[0].yaxis.grid(color = 'gray', linestyle = 'solid')
        ax[0].xaxis.grid(color = 'gray', linestyle = 'solid')
        ax[0].set_axisbelow(True)
        ax[0].legend(loc = 'upper left', shadow = False, fontsize = 'x-large', prop={"size":8})
        ax[0].set(ylabel='Entries')
        ax[0].set_xlim([0,1])
        
        ax[1].bar(bins[:-1]+(bins[1]-bins[0])/2, (hist_s-hist_s_train)/hist_s_train, width=binWidth, align='center', label='signal', 
                  color='cornflowerblue', alpha=1  , edgecolor='black')
        ax[1].bar(bins[:-1]+(bins[1]-bins[0])/2, (hist_b-hist_b_train)/hist_b_train, width=binWidth, align='center', label='background', 
                  color='darkmagenta',alpha=0.7, edgecolor='black')

        ax[1].legend(loc = 'upper right', shadow = False, fontsize = 'x-large', prop={"size":8})
        ax[1].set(ylabel='(Test-Train)/Train')
        #ax[1].set(ylabel='(gluons-quarks) / quarks')
        ax[1].yaxis.grid(color = 'gray', linestyle = 'solid')
        ax[1].xaxis.grid(color = 'gray', linestyle = 'solid')
        ax[1].set_xlim([0,1])
        
        plt.xlabel('Scoring')
        for ax in ax: ax.label_outer()
        plt.annotate(f"nLayers      = {nLayers}",           (0, max(np.concatenate((hist_b, hist_s), axis=None))))
        plt.annotate(f"nEpochs      = {self.nEpochs}",      (0, max(np.concatenate((hist_b, hist_s), axis=None))-0.5))
        plt.annotate(f"batchSize    = {self.batchSize}",    (0, max(np.concatenate((hist_b, hist_s), axis=None))-1))
        plt.annotate(f"learningRate = {self.learningRate}", (0, max(np.concatenate((hist_b, hist_s), axis=None))-1.5))
        
        plt.savefig(newDir+"output.pdf")
        plt.show()

        if (self.weightedROCs == True):
            fpr_train, fpr_test, tpr_train, tpr_test, trainAUC, testAUC, thresholds_train, thresholds_test = self.WeightedRocCurves(y_train, y_test, 
                                                                                                                                    #s_predictions_train, s_predictions_test, 
                                                                                                                                    predictions_train, predictions_test, 
                                                                                                                                    w_train, w_test)
        elif (self.weightedROCs == False):
            fpr_train, fpr_test, tpr_train, tpr_test, trainAUC, testAUC, thresholds_train, thresholds_test = self.unweightedRocCurves(y_train, y_test, 
                                                                                                                                      #s_predictions_train, s_predictions_test)
                                                                                                                                      predictions_train, predictions_test)
    
        plt.figure()
        #testLine  = plt.plot(tpr_test, fpr_test, label = f'test sample (AUC = {testAUC})')
        #trainLine = plt.plot(tpr_train, fpr_train, label = f'train sample (AUC = {trainAUC})')
        testLine  = plt.plot(tpr_test, fpr_test, label = f'Tower vars (test sample, AUC = {testAUC})')
        trainLine = plt.plot(tpr_train, fpr_train, label = f'Tower vars (train sample, AUC = {trainAUC})')
        if (self.addGammaROC == True):
            sys.path.insert(0, '/home/fede/Desktop/QT/gammaFits/')
            from gamma_plotter import gamma
            tpr_gamma, fpr_gamma, gammaAUC = gamma(self.min_pT, self.max_pT)
            gammaLine  = plt.plot(tpr_gamma, fpr_gamma, label = f'Gamma (AUC = {gammaAUC})')
        plt.plot([0, 1], [0, 1], linestyle="-", color="black")
        plt.xlabel('QCD jets efficiency (True positive rate)')
        plt.ylabel('Sto jets efficiency (False positive rate)')
        plt.title(f'ROC curves ( {label} )')
        plt.legend(loc='upper right')
        plt.grid()
        
        # add marker for 80% signal eff point
        xvalues = testLine[0].get_xdata()
        yvalues = testLine[0].get_ydata()
        
        #yvalues = testLine[0].get_ydata()
        
        xval = min(xvalues, key=lambda x:abs(x-0.8))
        
        idx  = np.where(xvalues==xval)
        yval = yvalues[idx][0]
        
        plt.scatter(xval, yval, color='r')
        plt.annotate("("+str(round(xval,3))+"; "+str(round(yval,3))+")", (xval+0.02, yval))
        plt.vlines(x = xval, ymin = 0, ymax = yval, colors = 'r', linestyle = 'dashed')
        plt.hlines(y = yval, xmin = 0, xmax = xval, colors = 'r', linestyle = 'dashed')

        if (self.addGammaROC == True):
            xvalues_gamma = gammaLine[0].get_xdata()
            yvalues_gamma = gammaLine[0].get_ydata()
            
            xval_gamma = min(xvalues_gamma, key=lambda x:abs(x-0.8))
            
            idx_gamma  = np.where(xvalues_gamma==xval_gamma)
            yval_gamma = yvalues_gamma[idx_gamma][0]
            
            plt.scatter(xval_gamma, yval_gamma, color='r')
            plt.annotate("("+str(round(xval_gamma,3))+"; "+str(round(yval_gamma,3))+")", (xval_gamma+0.02, yval_gamma))
            plt.vlines(x = xval_gamma, ymin = 0, ymax = yval_gamma, colors = 'r', linestyle = 'dashed')
            plt.hlines(y = yval_gamma, xmin = 0, xmax = xval_gamma, colors = 'r', linestyle = 'dashed')
        
        plt.annotate(f"nLayers      = {nLayers}",           (0, 0.9)    )
        plt.annotate(f"nEpochs      = {self.nEpochs}",      (0, 0.9-0.1))
        plt.annotate(f"batchSize    = {self.batchSize}",    (0, 0.9-0.2))
        plt.annotate(f"learningRate = {self.learningRate}", (0, 0.9-0.3))
        
        plt.savefig(newDir+"rocCurve.pdf")
        plt.show()
        
        # plot efficiencies
        plt.figure()
        plt.plot(thresholds_test, tpr_test, label = 'Signal efficiency')
        plt.plot(thresholds_test, fpr_test, label = 'Background efficiency')
        plt.xlabel('Cut')
        plt.ylabel('Efficiency')
        plt.legend(loc='best')
        plt.xlim([0, 1])
        plt.grid()
        plt.annotate(f"nLayers = {nLayers}",                (0.7, 0.7)    )
        plt.annotate(f"nEpochs = {self.nEpochs}",           (0.7, 0.7-0.1))
        plt.annotate(f"batchSize = {self.batchSize}",       (0.7, 0.7-0.2))
        plt.annotate(f"learningRate = {self.learningRate}", (0.7, 0.7-0.3))
        plt.savefig(newDir+"Effs.pdf")
        #plt.show()
        #return hist_s, hist_b, bins, binWidth

    def OnePtBin(self, df_train, df_test):
        print( "--------------------------------------------------------------------------")
        print(f"     Training/testing in only one pT bin: {self.min_pT}-{self.max_pT}GeV  ")
        print( "--------------------------------------------------------------------------")
        
        print(f"Testing set lenght is {len(df_test)}")
        print("--------------------------------")
        
        # define train/test samples
        # 'weight' is for sample weights  

        # gluon jets only
        #df_train = df_train[(df_train["truthLabelID"]==21 )]
        #df_test  = df_test[(df_test["truthLabelID"]==21 )]

        # quark jets only
        #df_train = df_train[(df_train["truthLabelID"] != 21 )]
        #df_test  = df_test[(df_test["truthLabelID"] != 21 )]

        # low stat test
        #df_test  = df_test.sample(frac = 0.1).reset_index(drop=True)

        # low mu
        #df_train = df_train[(df_train["mu"] < 22 )]
        #df_test  = df_test[(df_test["mu"] < 22 )]

        # high mu
        #df_train = df_train[(df_train["mu"] > 60 )]
        #df_test  = df_test[(df_test["mu"] > 60 )]

        X_train = df_train[self.inputMoments+["tag"]]
        y_train = df_train['tag']
        #w_train = df_train['weight']
        
        X_test  = df_test[self.inputMoments+["tag"]]
        y_test  = df_test['tag']
        #w_test  = df_test['weight']
        #mu_test = df_test['mu']
        
        #return X_train, y_train, w_train, X_test,  y_test,  w_test, mu_test
        return X_train, y_train, X_test,  y_test


    #def WeightedModelFit(self, X_train, y_train, w_train, X_test, y_test, w_test, results_dir):
    def WeightedModelFit(self, X_train, y_train, X_test, y_test, results_dir, valid_dataset):
        # build keras model
        model   = self.buildModel(self.nNodes)       
        nLayers = len(model.layers) - 2
        
        print("-----------------------------------")
        print("  Starting to fit the keras model  ")
        print("-----------------------------------")
        print(f"Parameters: -number of layers = {nLayers}")
        print(f"            -number of epochs = {self.nEpochs}")
        print(f"            -batch size       = {self.batchSize}")
        print(f"            -learning rate    = {self.learningRate}")
        print("-----------------------------------")
        print(model.summary())                              # Param# = n_neurons * ( n_inputs + 1)
                                                                                                                      
        # fit the keras model on the dataset
        history = model.fit(X_train, 
                            #y_train,
                            epochs           = self.nEpochs, 
                            batch_size       = self.batchSize,
                            validation_data  = valid_dataset)
                            #callbacks        = [callback],
                            #validation_split = 0.3)
        
        print("====================================================================================================")
        print("====================================== Evaluating keras model ======================================")
        print("====================================================================================================")
        
        # evaluate the keras model
        
        _,  train_accuracy = model.evaluate(X_train, 
                                            #y_train,
                                            batch_size = self.batchSize,
                                            verbose = 0)
        
        __, test_accuracy = model.evaluate(X_test, 
                                           #y_test,
                                           batch_size = self.batchSize,
                                           verbose = 0)
        
        print('Train accuracy: %.2f' % (train_accuracy*100))
        print('Test accuracy: %.2f' % (test_accuracy*100))

        model.save(results_dir+"model")
                                                                                                                      
        return model, history, nLayers

    def UnweightedModelFit(self, X_train, y_train, X_test, y_test, results_dir, valid_dataset):
        # build keras model
        model   = self.buildModel(self.nNodes)
        nLayers = len(model.layers) - 2

        print("-----------------------------------")
        print("  Starting to fit the keras model  ")
        print("-----------------------------------")
        print(f"Parameters: -number of layers = {nLayers}")
        print(f"            -number of epochs = {self.nEpochs}")
        print(f"            -batch size       = {self.batchSize}")
        print(f"            -learning rate    = {self.learningRate}")
        print("-----------------------------------")
        print(model.summary())                              # Param# = n_neurons * ( n_inputs + 1)

        # fit the keras model on the dataset
        history = model.fit(X_train,
                            #y_train,
                            epochs           = self.nEpochs,
                            batch_size       = self.batchSize,
                            validation_data  = valid_dataset)
                            #callbacks        = [callback],
                            #validation_split = 0.3)

        print("====================================================================================================")
        print("====================================== Evaluating keras model ======================================")
        print("====================================================================================================")

        # evaluate the keras model

        _,  train_accuracy = model.evaluate(X_train,
                                            #y_train,
                                            batch_size = self.batchSize,
                                            verbose = 0)

        __, test_accuracy = model.evaluate(X_test,
                                           #y_test,
                                           batch_size = self.batchSize,
                                           verbose = 0)

        print('Train accuracy: %.2f' % (train_accuracy*100))
        print('Test accuracy: %.2f' % (test_accuracy*100))

        model.save(results_dir+"model")

        return model, history, nLayers

    def loadModel(self, path, X_train, X_test):
        print("---------------------------------------------")
        print("     Loading trained model                   ")
        print("---------------------------------------------")

        model   = load_model(path)

        print("---------------------------------------------")
        print("     Model successfully loaded               ")
        print("---------------------------------------------")


        nLayers = len(model.layers) - 2

        _,  train_accuracy = model.evaluate(X_train,
                                            #y_train,
                                            batch_size = self.batchSize,
                                            verbose = 0)

        __, test_accuracy = model.evaluate(X_test,
                                           #y_test,
                                           batch_size = self.batchSize,
                                           verbose = 0)

        return model, nLayers

    def Predictions(self, model, X_train, X_test):
        # make class predictions with the model
        predictions_train = model.predict(X_train)
        predictions_test  = model.predict(X_test)

        return predictions_train, predictions_test

    def Eff_vs_Mu(self, results_dir, subdirName, y_test, predictions_test, mu_test):
        mu = mu_test.values.tolist()
        mu = [round(m) for m in mu]

        eff_05  = []
        nevs_05 = []

        eff_06  = []
        nevs_06 = []

        eff_07  = []
        nevs_07 = []

        #for t in [0.5, 0.6, 0.7]:
        for t in [0.5]:
            #for k in [20, 30, 40, 50, 60]:
            for k in np.linspace(20,60,21):
                s = []
                n = 0
                print(f"<mu> = {k}")
                iterator = tqdm(range(len(predictions_test)), desc='Calculating efficiency vs <mu> for testing set')
                for i in iterator:
                    if (y_test.iloc[i]==1 and mu[i] == k):
                        s.append(predictions_test[i][0])
                        n = n + 1
                #nevs_07.append(n)       

                #print(nevs_07)

                # calculate hs eff for a given threshold
                threshold = t
                #if len(s) == 0: hs_eff = 0
                #else: hs_eff = sum(i > threshold for i in s)/ len(s)       
                hs_eff = sum(i > threshold for i in s)/ len(s)       
                if t == 0.5:  eff_05.append(hs_eff); nevs_05.append(n)
                if t == 0.6:  eff_06.append(hs_eff); nevs_06.append(n)
                if t == 0.7:  eff_07.append(hs_eff); nevs_07.append(n)

                print(f"QCD eff for {t} threshold = {hs_eff}")     
                
        #print(nevs_07)
        #eff_07 = np.array(eff_07)
        ## calculate errors (binomial since it's an efficiency)
        #print(eff_07)
        #print(f"numerador = {eff_07*(1-eff_07)}")
        #print(f"denominador = {nevs_07}")

        eff_05 = np.array(eff_05)
        yerr_05 = np.sqrt( eff_05*(1-eff_05)/ nevs_05 )
        
        #eff_06 = np.array(eff_06)
        #yerr_06 = np.sqrt( eff_06*(1-eff_06)/ nevs_06 )

        #eff_07 = np.array(eff_07)
        #yerr_07 = np.sqrt( eff_07*(1-eff_07)/ nevs_07 )

        # plot efficiencies
        plt.figure()
        #plt.plot([20, 30, 40, 50, 60], eff_05, label = 'Threshold: 0.5')
        #plt.plot([20, 30, 40, 50, 60], eff_06, label = 'Threshold: 0.6')
        #plt.plot([20, 30, 40, 50, 60], eff_07, label = 'Threshold: 0.7')
        plt.errorbar(np.linspace(20,60,21), eff_05, yerr = yerr_05, label = 'Threshold: 0.5')
        #plt.errorbar(np.linspace(20,60,21), eff_06, yerr = yerr_06, label = 'Threshold: 0.6')
        #plt.errorbar(np.linspace(20,60,21), eff_07, yerr = yerr_07, label = 'Threshold: 0.7')
        plt.xlabel('<mu>')
        plt.ylabel('QCD efficiency')
        plt.legend(loc='best')
        #plt.ylim([0, 1])
        plt.grid()
        #plt.annotate(f"nLayers = {nLayers}",                (0.7, 0.7)    )
        #plt.annotate(f"nEpochs = {self.nEpochs}",           (0.7, 0.7-0.1))
        #plt.annotate(f"batchSize = {self.batchSize}",       (0.7, 0.7-0.2))
        #plt.annotate(f"learningRate = {self.learningRate}", (0.7, 0.7-0.3))
        #plt.savefig(newDir+"Effs.pdf")
        plt.show()

        return 0

# ========================================================================================================================
# =============================================== Aux functions ==========================================================
# ========================================================================================================================

def _int64_feature(value):
    return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def _float_feature(value):     
    return tf.train.Feature(float_list=tf.train.FloatList(value=[value])) 

def parse_single_event(event):
    # Function for writing an event as a TFRecord

    inputMoments = jetClassifier().inputMoments

    data = {} 	
    for i in inputMoments: 	
        data[i] = _float_feature(event.loc[i])
    data['tag']    = _float_feature(event.loc['tag'])    
    data['weight'] = _float_feature(event.loc['weight'])    
    features = tf.train.Features(feature = data)    
    out      = tf.train.Example(features = features) 	
    return out   

def _parse_function(proto):
    # Function for lifting the events written as TFRecords

    inputMoments = jetClassifier().inputMoments
    
    features = {}
    for i in inputMoments:
        features[i] = tf.io.FixedLenFeature([], tf.float32, default_value=0.0)
    features['tag']    = tf.io.FixedLenFeature([], tf.float32, default_value=0.0)  
    features['weight'] = tf.io.FixedLenFeature([], tf.float32, default_value=0.0)  
    parsed = tf.io.parse_single_example(proto,features) 	
    inputs = [] 	
    for tensor in parsed: 		
        if (tensor!='tag' and tensor!='weight'): 			
            inputs.append( tf.cast(parsed[tensor],tf.float32) )
    return inputs, parsed['tag'], parsed['weight']

# ========================================================================================================================
# =============================================== Main ===================================================================
# ========================================================================================================================

def main():
    
    # Initialize instance of class
    classifier = jetClassifier()
    print(f"======================================================================")
    print(f" ")
    print(f"    * pT bin: {classifier.min_pT}-{classifier.max_pT}GeV ")
    print(f"    * Train with sample weights: {classifier.trainWithWeights} ")
    print(f" ")
    print(f"======================================================================")
   
    #inFile = "/home/fede/Desktop/QT/highStats/weighted/Inputs_weighted.root"
    #inFile = "/home/fede/Desktop/QT/highStats/weighted/withRMS/Inputs_weighted_wRMS.root"
    inFile = "/home/fede/Desktop/QT/highStats/wGamma/Inputs_wGamma.root"
    
    # Make directory for saving plots and model 
    dirName = f"ForMeeting_{classifier.min_pT}_{classifier.max_pT}_weighted_Towers_vs_Gamma/"
    
    script_dir  = os.path.dirname(__file__)
    results_dir = os.path.join(script_dir, dirName)
    
    if not os.path.isdir(results_dir): os.makedirs(results_dir)

    # convert TTrees into DataFrames and split into train/testing sample
    X_train, X_test, y_train, y_test = classifier.treesToDataFrames(inFile, results_dir)

    # convert DataFrames into TFRecord with TFRecordWriter()
    train_data = f'wRMS_train_data_{classifier.min_pT}_{classifier.max_pT}GeV.tfrecord'
    valid_data = f'wRMS_valid_data_{classifier.min_pT}_{classifier.max_pT}GeV.tfrecord'
    test_data  = f'wRMS_test_data_{classifier.min_pT}_{classifier.max_pT}GeV.tfrecord'

    if classifier.trainWithTFRecord == True:
        X_train, X_valid, y_train, y_valid = train_test_split(X_train, y_train, test_size=0.3, shuffle= False, random_state = 1)

        w_train = X_train['weight']
        w_test  = X_test['weight']

        if classifier.writeTFRecord == True:
            n = 0
            with tf.io.TFRecordWriter(train_data) as writer: 
                iterator = tqdm(range(y_train.shape[0]), desc='Writing training events')
                for i in iterator:
                    event   = X_train.iloc[i]
                    example = parse_single_event(event)
                    writer.write(example.SerializeToString())
                    n+=1
                writer.close()
                print(f"Wrote {n} events to TFRecord")

            n = 0
            with tf.io.TFRecordWriter(valid_data) as writer: 
                iterator = tqdm(range(y_valid.shape[0]), desc='Writing validation events')
                for i in iterator:
                    event   = X_valid.iloc[i]
                    example = parse_single_event(event)
                    writer.write(example.SerializeToString())
                    n+=1
                writer.close()
                print(f"Wrote {n} events to TFRecord")

            n = 0
            with tf.io.TFRecordWriter(test_data) as writer: 
                iterator = tqdm(range(y_test.shape[0]), desc='Writing testing events')
                for i in iterator:
                    event   = X_test.iloc[i]
                    example = parse_single_event(event)
                    writer.write(example.SerializeToString())
                    n+=1
                writer.close()
                print(f"Wrote {n} events to TFRecord")

        X_train = tf.data.TFRecordDataset([train_data])
        X_train = X_train.map(_parse_function)
        X_train = X_train.batch(classifier.batchSize)

        X_valid = tf.data.TFRecordDataset([valid_data])
        X_valid = X_valid.map(_parse_function)
        X_valid = X_valid.batch(classifier.batchSize)

        X_test = tf.data.TFRecordDataset([test_data])
        X_test = X_test.map(_parse_function)
        X_test = X_test.batch(classifier.batchSize)

    # build and fit the Keras model
    if (classifier.trainWithWeights == True):
        if (classifier.loadTrainedModel) == False:
            #fittedModel, history, nLayers = classifier.WeightedModelFit( X_train, y_train, w_train, X_test, y_test, w_test, results_dir )
            fittedModel, history, nLayers = classifier.WeightedModelFit( X_train, y_train, X_test, y_test, results_dir, X_valid )
        elif (classifier.loadTrainedModel) == True:
            path = f'/home/fede/Desktop/QT/highStats/{classifier.min_pT}_{classifier.max_pT}_weighted_weightedROCS_wTimeRMS/model/'
            fittedModel, nLayers = classifier.loadModel(path, X_train, X_test)
            history = []

        #make predictions and plots
        predictions_train, predictions_test = classifier.Predictions(fittedModel, X_train, X_test)
        classifier.PerformancePlots(results_dir, "output/", history, nLayers, y_train, 
                                                                              w_train, 
                                                                              predictions_train, 
                                                                              y_test,  
                                                                              w_test,  
                                                                              predictions_test)
    elif (classifier.trainWithWeights == False):
        if (classifier.loadTrainedModel) == False:
            fittedModel, history, nLayers = classifier.UnweightedModelFit( X_train, y_train, X_test, y_test, results_dir, X_valid )
        elif (classifier.loadTrainedModel) == True:
            path = f'/home/fede/Desktop/QT/highStats/{classifier.min_pT}_{classifier.max_pT}_weighted_weightedROCS/model/'
            fittedModel, nLayers = classifier.loadModel(path, X_train, X_test)
            history = []
        
        #make predictions and plots
        predictions_train, predictions_test = classifier.Predictions(fittedModel, X_train, X_test)
        classifier.PerformancePlots(results_dir, "output/", history, nLayers, y_train, 
                                                                              #w_train, 
                                                                              predictions_train, 
                                                                              y_test,  
                                                                              #w_test,  
                                                                              predictions_test)
        #classifier.Eff_vs_Mu(results_dir, "output/", y_test, predictions_test, mu_test)

    print("                                                                                                    ")  
    print("                                                                                                    ")  
    print("====================================================================================================")  
    print("====================================== The end =====================================================")
    print("====================================================================================================")

#------------------------------------------------------------------------------------------------------------------------------------------------------------------------

if (__name__ == '__main__'):
    main()
