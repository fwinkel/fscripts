from iminuit import Minuit
#import ROOT
import numpy as np
import matplotlib.pyplot as plt
import os
import random as rnd
#plt.rc('xtick', labelsize=20)
#plt.rc('ytick', labelsize=20)
#plt.rcParams['mathtext.fontset'] = 'custom'
#plt.rcParams['font.family'] = 'serif'
#plt.rcParams['font.serif'] = ['Arial'] + plt.rcParams['font.serif']
#plt.rcParams['mathtext.bf'] = 'Arial:italic:bold'
#legend_properties = {'weight':'bold'}

############# Functions ####################################

sigma = 1
a0 = 2
a1 = 1

x = np.array( [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] )
y = np.array( [ rnd.gauss(i*a0 + a1, sigma) for i in x ] )
yerr = np.ones(len(y))

#plt.errorbar(x, y, yerr, fmt='o')
#plt.show()

def model_func(X, a, b):
  return a*X + b

# Chisq func 
def chisq_func(a, b):
    model = model_func(x, a0, a1)
    chisq = np.sum( ((model - y) / sigma)**2 )
    return chisq

############ Fitter ########################################

chisq_func.errordef = Minuit.LEAST_SQUARES

#chisq_func = LeastSquares(data_x, data_y, data_yerr, line)

m = Minuit(chisq_func, a=0, b=0)

m.migrad()  # run optimiser
m.hesse()   # run covariance estimator
print('Best-fitting a: %.4f ± %.4f'%(m.values['a'], m.errors['a']))
print('Best-fitting b: %.4f ± %.4f'%(m.values['b'], m.errors['b']))

#print('Corr(S, C): %.2f'%(m.covariance.correlation()['S']['C']))
#
## Check if function minimum is valid
#print('Valid chisq minimization: %r'%m.valid)
#
## Check if covariance matrix is accurate
#print('Valid covariance matrix: %r'%m.accurate)
#
#print('chisq / ndof at minimum: %.2f / %d'%(m.fval, nBins - 2))
#
#print('#############################\n')
#
## Plot the JER, along with the best fit to SNC and uncertainty band
#S_fit = m.values['S']
#C_fit = m.values['C']
#
## Make a range of pT values for the fitted results with the same range (15-3000 GeV) and spacing (1 GeV) as the output produced by ToolsJEScombination
#pT_fit = np.arange(15, 3000) + 0.5
#
## nominal snc fit
#snc_fit_nom = snc_model_func(pT_fit, S_fit, N_full[Rjet][iEta], C_fit)
#
### Plot the MC JER, and the SNC fit and error band
##fig = plt.figure(figsize=(9, 7))
##ax = fig.add_subplot(1,1,1)
##ax.set_xlabel(r"p$_T^\mathrm{jet}$ [GeV]", fontsize=22)
##ax.set_ylabel('Jet energy resolution, $\sigma(p_T) / p_T$', fontsize=22)
##ax.set_xscale('log')
##ax.set_ylim(0, 0.6)
##ax.set_xlim(15, 3000)
##ax.set_yticks(np.arange(0, 0.6, 0.02), minor=True)
##ax.errorbar(pT, jer, jer_unc, marker='^', color='black', linestyle='', markerfacecolor='none', label='Dijets', capsize=2)
##ax.plot(pT_fit, snc_fit_nom, color='black', label = 'SNC Fit')
##ax.fill_between(pT_fit, snc_fit_nom-snc_fit_statunc, snc_fit_nom + snc_fit_statunc, color='blue', alpha=0.7, label='Statistical uncertainty\n(from Dijets MC JER meas.)')
##plt.figtext(0.67, 0.90, r"$\mathbf{ATLAS}$ Internal", fontsize=20)
##plt.figtext(0.67, 0.85, "13 TeV, 36.1 fb$^{-1}$", fontsize=20)
##if Rjet == 'R02': plt.figtext(0.14, 0.90, "anti-k$_t$ R=0.2, LCW+JES", fontsize=20)
##if Rjet == 'R06': plt.figtext(0.14, 0.90, "anti-k$_t$ R=0.6, LCW+JES", fontsize=20)
##plt.figtext(0.14, 0.85, eta_labels[iEta], fontsize=20)
##
### Make a legend and set the order in which plotted objects appear
##handles, labels = plt.gca().get_legend_handles_labels()
##order = [2,0,1]
##plt.legend([handles[idx] for idx in order],[labels[idx] for idx in order], loc='center right', prop=dict(weight='bold', size = 20))
##
### Save the figure
##plt.tight_layout()
##plt.savefig('Plots/snc_fit_%s_eta%d.pdf'%(Rjet, iEta+1))
##plt.savefig('Plots/snc_fit_%s_eta%d.png'%(Rjet, iEta+1))
##
### Save the fit results to a txt file
##output_arr = np.zeros((len(pT_fit), 3))
##output_arr[:,0] = pT_fit-0.5
##output_arr[:,1] = pT_fit+0.5
##output_arr[:,2] = snc_fit_nom
##
### Make sure the dir structure exists to contain the ostput tables
##if not os.path.exists('Tables_MC_JER/%s_eta%d'%(Rjet.replace('0', ''), iEta+1)):
##  os.makedirs('Tables_MC_JER/%s_eta%d'%(Rjet.replace('0', ''), iEta+1))
##
##np.savetxt('Tables_MC_JER/%s_eta%d/JERfitResult_MC.txt'%(Rjet.replace('0', ''), iEta+1), output_arr, fmt='%.6e')
    
  
