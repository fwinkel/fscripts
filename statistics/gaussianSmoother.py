###########################################################################################################
#  Script for smoothing. Wasn't made by me and I haven't fully read it, but might be useful in the future # 
###########################################################################################################

import argparse, os, math, array
import ROOT

def RunSmoothing():

  # Some settings  
  xmax = 1000.
  xmin = 20. #???
  #kernel_fraction = 10
  kernel_fraction = 0.6 #0.3
  pTSmooth = 10

  print ("-------------------------------------------------------------------------------------------")
  print ("   Smoothing Tool:")
  print ("   based on Jeff's smoothing tool in the GSC package, using stat errors for the smoothing ")
  print ("--------------------------------------------------------------------------------------------")

  #---------------------------------------------------------------------------------

  ## Make output file ##
  outFileName = outDir+"OOC_GaussianSmoother_" + args.jetR + ".root"
  outputFile = ROOT.TFile(outFileName, "RECREATE")
  
  # open file
  myFile = ROOT.TFile(args.input,"read")
  # pick up the histogram
  h = myFile.Get(myFile.GetListOfKeys()[0].GetName())
  print ("Opening histogram ", h.GetName())
  if not h:
      print ("Could not retrieve histogram, exiting")
      exit(0)
  
  ## styling original histogram
  h.GetXaxis().SetTitle("p_{T}^{Rscan} [GeV]")
  #h.SetBinError(4,h.GetBinError(4)*1000)

  xbins = [h.GetBinLowEdge(i) for i in range(1,h.GetNbinsX()+2)]  #h.GetNbinsX()+2
  print (xbins)
  ## define a TGraph (to re-use Jeff's smoother). Will want to change it to a HistoSmoother rather than a TGraphSmoother...meeh
  graph = ROOT.TGraphErrors(h)

  ## Create smoother tool ##
  MySmoother = Smoother("Smoother",xmin,xmax,xbins,kernel_fraction)
  ## make the smoothed histogram
  hsmo = MySmoother.MakeSmoothHisto(graph)
  
  ## Draw results and save in output rootfile ##
  can = ROOT.TCanvas()
  can.SetLogx()
  can.cd()
  can.SetGridx()
  can.SetGridy()
  h.Draw()
  hsmo.SetLineColor(ROOT.kRed)
  hsmo.Draw("HIST C SAME")

  leg = ROOT.TLegend(0.7,0.2,0.9,.3)
  leg.AddEntry(h,"original")
  leg.AddEntry(hsmo,"smoothed")
  leg.Draw("same")

  ltex = ROOT.TLatex()
  ltex.SetTextSize(0.02)
  #ltex.DrawLatex(30.,0.018,"OOC Component: 2015+2016 data")
  #ltex.DrawLatex(100.,0.012,"AntiKt"+args.jetR+"Topo")
   
  can.Print("Smoothed_OOCcomponent_"+args.jetR+".pdf")
  outputFile.cd()
  hname = h.GetName().replace("JETDEF","smoothed_AntiKt")+args.jetR+"Topo"
  
  print (hname)
  hsmo.SetNameTitle(hname,hname)
  hsmo.Write()
  outputFile.Close()
  print ("Finished smoothing, produced root file", outFileName)
 

#---------------------------------------------------------------------------------
############################## Smoothing class ##############################
## This class provides a first smoothing of each response TGraph as a function of pt ##
class Smoother():

  def __init__(self, name, minX, maxX, xbins,kernelfraction=100): #kernelfraction=5
    self.name = name
    self.logX = True
    self.logY = False
    self.fraction=kernelfraction
    self.minX=minX
    self.maxX=maxX
    self.kernel = 0
    self.bins = xbins

  #---------------------------------------------------------------------------------
  ## Get weight of point, the inverse of the y error squared ##
  def GetW(self, graph, i):
    if graph.GetN() <= i:
      print ("Cannot access point", i, "as the graph only has", graph.GetN(), "is.")
      exit(1)
    if (graph.GetEY()[i]==0):
       print ("ERROR: A point has error = 0, x={:.2f} y={:.2f}".format(graph.GetX()[i],graph.GetY()[i]))
       exit(1)
    return 1.0/(graph.GetEY()[i]**2)

  def MakeSmoothHisto(self, graph, Nbins=40, xmin = None, xmax = None):

    ### Set the X-variable edges using the lowest pt TGraph ##
    if (xmin == None):
      xmin = self.minX-0.02*self.maxX
    if (xmax == None):
      xmax = self.maxX*1.05

    bins = array.array('d',self.bins)
    #h = ROOT.TH1F( graph.GetName()+"_smoothed", graph.GetName()+"_smoothed", Nbins, xmin, xmax)
    h = ROOT.TH1F(graph.GetName()+"_smoothed", graph.GetName()+"_smoothed", graph.GetN(), bins)
    h.SetStats(0)

    print ("M: graph is: ", graph)
    print ("M: graph has ", graph.GetN(), " points")
    g_ey = graph.GetEY()
    
    print ("M: errors on the graph are: ")
    for p in range(0,len(g_ey)):
        print (g_ey[p])
    
    errorSum = 0
    for i in range(0,graph.GetN()):
      errorSum += g_ey[i]

    extentfraction = self.fraction
    if( errorSum<0.05):
      extentfraction = 10;
    else:
      extentfraction = self.fraction
    
    for i in range(1, h.GetNbinsX()+1):
      x = h.GetBinCenter(i);
      #binCont, binErr = self.GetValueAndError(graph, x, extentfraction);
      binCont, binErr = self.GetSmoothedValue(graph, x, extentfraction);

      h.SetBinContent(i, binCont)
      h.SetBinError(i, 0.00001);

    h.GetXaxis().SetTitle( graph.GetXaxis().GetTitle() )
    h.GetYaxis().SetTitle( graph.GetYaxis().GetTitle() )

    return h;

  #---------------------------------------------------------------------------------
  ## Calculate the smoothed bin content and error for one point of a TGraph ##
  def GetValueAndError(self, g, x, extentfraction):
    if (g.GetN() == 0):
      print ("Empty graph")
      exit(1)
    if (g.GetN() == 1):
       return g.GetY()[0], g.GetEY()[0]

    g_x = g.GetX()
    g_y = g.GetY()
    g_ey = g.GetEY()
    xmin = g_x[0]
    xmax = g_x[ g.GetN()-1]
    Dx = xmax-xmin
    self.kernel = Dx/extentfraction

    if(x < xmin):
      return g.GetY()[0], g.GetEY()[0]
    elif(x > xmax):
      x = xmax


    if self.logX and xmin <= 0:
      print ("Error, attempting to set x-axis to log scale with non-positive values. Exiting")
      exit(1)

    sumw, sumwy = 0, 0
    ## Loop over each pair of pt points in the TGraph
    ## Contributing a weighted value to the bin content based on the distance to the point and error
    for i in range(0, g.GetN() ):
      x1 = g_x[i]
      if self.logX:
          x1 = math.log(x1)
      y1 = g_y[i]
      w1 = self.GetW(g,i)


      for j in range(i+1, g.GetN() ):
        x2 = g.GetX()[j]
        if self.logX:
          x2 = math.log(x2)
        y2 = g_y[j]
        w2 = self.GetW(g,j)


        if (x1 == x2):
          print ("Not yet implemented points with same x values")
          exit(1)

        ## Calculate weighted y value ##
        maxDx = max( abs(x1-x), abs(x2-x) )
        wGaus = ROOT.TMath.Gaus(maxDx/self.kernel)
        mu_x = (w1*x1+w2*x2)/(w1+w2)
        mu_y = (w1*y1+w2*y2)/(w1+w2)
        dmu_y = math.sqrt(1.0/(w1+w2))

        # option 1
        k = (y2-mu_y)/(x2-mu_x)
        V_y2 = g_ey[j]**2 - dmu_y**2
        dk = math.sqrt(V_y2)/(x2-mu_x)

        # option 2 - equivalent
        #double k=(y2-y1)/(x2-x1), dk=sqrt(pow(g.GetEY()[i],2)+pow(g.GetEY()[j],2))/(x2-x1);

        e = math.sqrt( (dk*(x-mu_x))**2 + dmu_y**2 )
        w = 1.0/e/e;
        y = k*(x-mu_x)+mu_y
        w *= wGaus;
        sumwy += w*y
        sumw += w

    return sumwy/sumw, math.sqrt(1.0/sumw)

  ## Another smoother used in insitu Rscan
  def GetSmoothedValue(self,g,x,pTSmooth):
    if (g.GetN() == 0):
      print ("Empty graph")
      exit(1)
    if (g.GetN() == 1):
       return g.GetY()[0], g.GetEY()[0]

    g_x = g.GetX()
    g_y = g.GetY()
    g_ey = g.GetEY()
    for i in range(0,g.GetN()):
        if g_ey[i]==float(0):
            g_ey[i]=float('inf')
    #xmin = [ g_x[i] for i in range(0, g.GetN()) if g_ey[i] is not float('inf')][0]
    xmin = g_x[2]
    xmax = g_x[ g.GetN()-1]

    if(x < xmin):
      #return g.GetY()[2], g.GetEY()[2]
      x = xmin
    elif(x > xmax):
      x = xmax

    sumw, sumwz = 0, 0
    ## Contributing a weighted value to the bin content based on the distance to the point and error
    for i in range(0, g.GetN() ):
      x1 = g_x[i]
      y1 = g_y[i]
      dx = (x-x1)/pTSmooth
      if self.logX:
          dx /= x
      dr = abs(dx)
      w = ROOT.TMath.Gaus(dr)
      w*=1./math.pow(g_ey[i],2)
      sumw += w
      sumwz += w*y1
    
    return sumwz/sumw, math.sqrt(1./sumw) 


#---------------------------------------------------------------------------------
if __name__ == "__main__":

  parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument("-b", dest='batchMode', action='store_true', default=False, help="Batch mode for PyRoot.")
  parser.add_argument("--debug", dest='debug', action='store_true', default=False, help="More verbose for debugging.")
  parser.add_argument("--input", dest='input', required=True, default="", help="Input file name.")
  parser.add_argument("--ptMin", dest='ptMin', type=int, default=20, help="Minimum pt")
  parser.add_argument("--jetR", dest='jetR', default="6LC", help="Jet radii")
  args = parser.parse_args()

  outDir = os.path.dirname(args.input)

  #AtlasStyle.SetAtlasStyle()
  ROOT.gStyle.SetOptStat(0)
  ROOT.gErrorIgnoreLevel = ROOT.kWarning
  # Always do batch mode!
  ROOT.gROOT.SetBatch(True)

  RunSmoothing()
  print ("Done smoothing")
