import ROOT

# Open the root file
inFile = ROOT.TFile("signal.root", "READ")

# Get the TTree
tree = inFile.Get("analysis")

# List of variables
features = [
    "Tracks_d0",
    "Tracks_z0SinTheta",
    "Tracks_deta",
    "Tracks_dphi",
    "Tracks_qOverP",
    "Tracks_IP3D_signed_d0_significance",
    "Tracks_IP3D_signed_z0_significance",
    "Tracks_phiUncert",
    "Tracks_thetaUncert",
    "Tracks_qOverPUncert",
    "NumPixHits",
    "NumSCTHits",
    "NumIPLHits",
    "NumNIPLHits",
    "NumIPLSharedHits",
    "NumIPLSplitHits",
    "NumPixSharedHits",
    "NumPixSplitHits",
    "NumSCTSharedHits",
    "NumPixHoles",
    "NumSCTHoles",
    "Tracks_leptonID",
    'CaloJet_pt',
    'CaloJet_eta'
]

pdfName = "variables.pdf"

# Create canvas for plotting
c = ROOT.TCanvas("canvas", "Plots", 800, 600)
c.cd()
c.Print(pdfName+"[")

# Loop over variables
for feature in features:
    # Create histogram
    hist = ROOT.TH1F(feature, feature, 100, tree.GetMinimum(feature), tree.GetMaximum(feature))

    # Fill histogram from TTree
    tree.Draw(feature + ">>" + feature)

    # Draw histogram on canvas
    hist.Draw()

    c.Print(pdfName)

c.Print(pdfName+"]")

# Close the root file
inFile.Close()

