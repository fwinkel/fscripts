import torch
import os
import types
import uproot
import numpy as np
import awkward as ak
from torch_scatter import segment_coo, scatter
from functools import partial

#############################################################################################################
############### Functions ###################################################################################
#############################################################################################################

def toNumpy(awkarray):
    """ ak.ravel will give back a flat view of the array"""
    return np.asanyarray( ak.ravel( awkarray) )
    
def toTorchTensor(awkarray):
    """ ak.ravel will give back a flat view of the array                                                   """    
    """ then we translate to a numpy array so torch can view the underlying data :                         """       
    """ (When running on CPU this is a zero copy operation, the same data is viewed by torch and awkarray) """      
    return torch.tensor( np.asanyarray( ak.ravel( awkarray) ))                                                    

def buildSCalesOffsets():
    # init empty tensors :
    scales  = torch.zeros( nFeatures)
    offsets = torch.zeros( nFeatures)
    for i,f in enumerate(features):
        arrayFeature = getattr(tensors,f)
        scales[i]    = 1./arrayFeature.std()
        offsets[i]   = -arrayFeature.mean()*scales[i]

    return scales, offsets

def toOffset( numbers ):
    """We first need to prepare a table of 'offset'. 
       For example such that for a graph g, the corresponding node features in the feature array will start at position nodeOffsets[g] until position nodeOffsets[g+1
       We can get such a table from the array of the numbers of nodes per graph (and from the fact that the features are properly stacked).
    """
    numbers = np.asanyarray(ak.ravel(numbers)) # convert to flat numpy array
    offsets = np.zeros(numbers.shape[0]+1, dtype=numbers.dtype)
    offsets[1:]  = np.cumsum(numbers)
    return offsets

# We now write a function which extracts all necessary info from all the graphs between position start and position stop
def batch(start, stop):
    """We now write a function which extracts all necessary info from all the graphs between position start and position stop"""
    # node features
    n0 = tensors.nodeOffsets[start]
    n1 = tensors.nodeOffsets[stop]

    # retrieve features between n0 and n1
    arrayList=[]
    for i,f in enumerate(features):
        arrayList.append( getattr(tensors, f)[n0:n1] )

    # build a compact 2d array by stacking individual features:
    inputs = torch.stack(arrayList, dim=1)

    # ---
    # same for the edges :
    n0 = tensors.edgeOffsets[start]
    n1 = tensors.edgeOffsets[stop]

    # retrieve features between n0 and n1
    recieverI = tensors.Tracks_recieverI[n0:n1]
    senderI   = tensors.Tracks_senderI[n0:n1]

    edges =  torch.stack([senderI, recieverI], dim=1)

    # We'll also need the number of nodes per graph
    numNodes = tensors.NumberOfTracks[start:stop]
    # And the number of edges per graph
    numEdges = tensors.NumberOfEdges[start:stop]

    # return everything for this batch
    return inputs, edges, numNodes, numEdges

def lossFunction():
    """ Binary cross entropy loss function """
    dE = torch_nn.BCELoss()
    return dE

#############################################################################################################
############### Classes #####################################################################################
#############################################################################################################

class Normalization(torch.nn.Module):
    """Applies a linear normalization to the inputs : y=scale*x+offset where
    scale and offset are user given, fixed parameters.
    """
    def __init__(self, scales, offsets, **args):
        super().__init__(**args)
        # declare some NON trainable parameters (pass the requires_grad=False option)
        self.scales  = torch.nn.Parameter(scales,  requires_grad=False)
        self.offsets = torch.nn.Parameter(offsets, requires_grad=False)

    def forward(self, x):
        u= self.scales * x + self.offsets
        return u

# write a Module dedicated to translate indices :
class ShiftNodeIndices(torch.nn.Module):
    """ The edges are given as indices in each graph. But the data is organised by batches
          * How to shift the value of the node indices in the graph so they correspond to the indices in the batch?
    
        Write a Module dedicated to translate indices :
    """
    def forward(self, edgeI, numNodesPerGraph, numEdgesPergraph):
        # inputs refer to the current batch :
        # edgI is a 2D array of shape (nEdgesIntheBatch, 2) as build in the batch function above
        # numEdgesPergraph and numNodesPerGraph are 1D of shape (nGraphsIntheBatch,)

        # We need to recalculate the node offsets in the batch.
        # Intialize them at 0 :
        offset = torch.zeros( len(numNodesPerGraph), dtype=int )

        # Let's use the cumsum() function (cumsum = cummulative sum )
        cs = torch.cumsum( numNodesPerGraph , dim=0)
        #input("???")
        # careful ! cs = [n0 , n0+n1, n0+n1+n2, ...]
        # but what we want is [0, n0, n0+n1, ...]
        offset[1:] += cs[:-1]
        # now we have offset == [0, n0, n0+n1, ...]

        # finally we need to add these offsets to each of the corresponding edges
        # so we duplicate the offsets for each of the edges :
        offset_atedges = torch.repeat_interleave(offset, numEdgesPergraph)

        edgeI = edgeI+offset_atedges.view(-1,1)

        return edgeI


class MLP(torch.nn.Module):
    """We'll use 3 MLP in the full GNN. So we write a Module to represent mlp using several nn.Linear and a nn.Sequential, following something like :
                          mlp = Sequential( Linear(n0,n1), activation(), 
                                            Linear(n1,n2), activation(), 
                                            Linear(n2,n3), activation(), 
                                            ...)"""
    def __init__(self, numInput, layers=[], activation=torch.nn.Mish(), last_activation=None):
        super().__init__()
        nin = numInput
        seq = []
        for nout in layers:
            seq += [ torch.nn.Linear(nin, nout), activation ]
            nin = nout
        if last_activation is None: last_activation=activation
        seq[-1] = last_activation

        self.model = torch.nn.Sequential(*seq)

    def forward(self, x):
        return self.model(x)


#############################################################################################################
############### Reading and understanding inputs ############################################################
#############################################################################################################

# reading the input
filename = "mc20_13TeV_tracksIndexed.root"

# Get the TTree inside it with uproot.
tree = uproot.open(filename+":analysis")

features = ['Tracks_pt', 
            'Tracks_eta', 
            'Tracks_phi', 
            'Tracks_e', 
            #'Tracks_d0', 
            #'Tracks_z0', 
            #'NumPixHits', 
            #'NumPixHoles', 
            #'NumPixSharedHits', 
            #'NumPixDeadSensors', 
            #'NumSCTHits', 
            #'NumSCTHoles',
            #'NumSCTSharedHits', 
            #'NumSCTDeadSensors'
           ]

auxVars  = ['CaloJet_e',
            'CaloJet_pt', 
            'CaloJet_eta', 
            'NumberOfTracks',
            'NumberOfEdges'
           ]

edges    = ['Tracks_recieverI', 'Tracks_senderI']

"""
targets = ['jet_true_E']
"""

nFeatures = len(features)

# Load all the above branches
#arrays = tree.arrays(features+edges+targets+auxVars)
arrays = tree.arrays(features+edges+auxVars)

# List of all loaded branches:
"""branches = arrays.fields
for b in branches: print(b) 
"""

# Access the data by branch name:
"""
print(arrays.Tracks_z0)
"""

# Arrays contain "awkward arrays ": these are "extension" of numpy arrays which allows multi-dim arrays with inner dimensions of varying size,  
# for example like in vector<vector<float>>.

# In this file constituents/nodes are stored by Event

# For example, compare:
"""
print(arrays.Tracks_e[0])       # => energies of reco constituents in event 0
print(arrays.Tracks_e[1])       # => energies of reco constituents in event 1
print(arrays.NumberOfTracks[0]) # => num of constituents for each jet in event 0
"""

## The energies  of constituents of jet 0 in event 0 are:
"""
nc0 = arrays.NumberOfTracks[0][0] # number of constits in event0, jet 0
arrays.Tracks_e[0][0:nc0]
# since array[a][b] can be written array[a,b], the above is also :
print("E of constits in jet 0 of event 0 = ", arrays.Tracks_e[0, 0:nc0 ], "sums to : ",ak.sum(arrays.Tracks_e[0, 0:nc0 ]))
print("E from jet 0 in event 0: ",arrays.CaloJet_e[0,0])
"""

# Explore the content of other variables in arrays 
# Also check the node indices on edges :
arrays.Tracks_recieverI[0] # --> the list of recieving node indices in event 0
arrays.Tracks_senderI  [0] # --> the list of sending node indices in event 0

# The total number of edges is thus :
#  (ravel returns a flat view on the data)
ne1 = len(ak.ravel(arrays.Tracks_senderI) )
# and should be the same as
ne2 = ak.sum(arrays.NumberOfEdges)
print("Edge test passed? ",ne1==ne2)

##############################################################################################################
################ Conversion to pytorch tensor ################################################################
##############################################################################################################

toTorchTensor(arrays.Tracks_phi)

# Let's automatically convert all arrays and collect them into a simple python object:
tensors = types.SimpleNamespace() # a basic python object

# This just sets tensors.bla = toTorchTensor(bla):
for f in arrays.fields: setattr(tensors, f, toTorchTensor(arrays[f]))

# While at it, let's also build tensor view on num of nodes, constits and jet E:
tensors.numNodes = toTorchTensor(arrays.NumberOfTracks)
tensors.numEdges = toTorchTensor(arrays.NumberOfEdges )
tensors.jet_E    = toTorchTensor(arrays.CaloJet_e     )

##############################################################################################################
################ Input transformation ########################################################################
##############################################################################################################

############### Feature normalization #######################################################################
scales, offsets = buildSCalesOffsets()

############### Indices manipulation : batch preparation ####################################################

# Node offsets from the number of constit per jet :
tensors.nodeOffsets = torch.tensor(toOffset(arrays.NumberOfTracks))

# Cross check : let's look at the 1st constits of jet 4 :
n0, n1 = tensors.nodeOffsets[4], tensors.nodeOffsets[4+1]

# similarly we'll need the edges offsets :
tensors.edgeOffsets = torch.tensor(toOffset(arrays.NumberOfEdges))

# Do a basic test :
feat39, edg39, nnod39, nedg39 = batch(3,9)
print("Features shape ok? ",feat39.shape == ( tensors.NumberOfTracks[3:9].sum(), nFeatures )) 

################ Indices manipulation : translating indices in graph to indices in batch ####################################################

shiftNI = ShiftNodeIndices()
# check the results :
shiftNI(edg39, nnod39, nedg39)

##############################################################################################################
################ Model architecture ##########################################################################
##############################################################################################################

############### MLP #########################################################################################

# First we need a module which performs the average of features over connected nodes
class AverageConnNodes(torch.nn.Module):
    """Performs aggregation of nodes connected in a graph. 
    """
    def forward(self, edgI, nodeFeatures):
        senderI   = edgI[:,0]
        recieverI = edgI[:,1]

        # take node features at each possible sending node: 
        featuresAtSendNode = nodeFeatures[senderI] 
        
        nNodes = nodeFeatures.shape[0]

        # we use the torch_scatter.scatter() function to sum the features at all possible sending nodes
        # onto the position of recieving nodes
        avgFeatures = scatter(featuresAtSendNode,recieverI, dim_size=nNodes, reduce='mean' , dim=0)
        return avgFeatures


# Next a message passing module combining a MLP and the `AverageConnNodes`
class MessagePassing(torch.nn.Module):
    def __init__(self, numInput, layers=[], activation=torch.nn.Mish(), last_activation=None):
        super().__init__()
        self.avgNodes = AverageConnNodes()
        self.mlp      = MLP(numInput,layers,activation,last_activation)

    def forward(self, nodeFeatures, edgI):

        avg = self.avgNodes(edgI, nodeFeatures)
        out = self.mlp(avg)

        return out


############## Complete GNN #################################################################################

# Let's define a very simple activation function 

class lastActivation(torch.nn.Module):
    """Define an activation giving values between [0,1]"""
    def forward(self,x):
        return torch.sigmoid(x)

class GNN(torch.nn.Module):
    def __init__(self, ):
        super().__init__()
        self.norm     = Normalization(*buildSCalesOffsets())
        self.shiftNI  = ShiftNodeIndices()
        #self.preMLP   = MLP(nFeatures, [256,128,64,32] )
        self.preMLP   = MLP(nFeatures, [128,64,32] )
        self.mpBlock1 = MessagePassing(32, [128,64,32] )
        self.mpBlock2 = MessagePassing(32, [128,64,32] )
        self.postMLP  = MLP( 32, [256,128,64,1], last_activation=lastActivation() )

    def forward(self, nodeFeatures_raw, edgeI_pergraph, numNodesPerGraph, numEdgesPergraph):
        nodeFeatures = self.norm(nodeFeatures_raw)
        edgeI        = self.shiftNI(edgeI_pergraph, numNodesPerGraph, numEdgesPergraph)
        y = self.preMLP(nodeFeatures)
        y = self.mpBlock1(y, edgeI)
        y = self.mpBlock2(y, edgeI)
        y = self.postMLP(y)

        return y

gnn = GNN()

# Let's try it :
calibFactors = gnn(feat39, edg39, nnod39, nedg39)

############### Loss Function ################################################################################

def lossFunction():
    """ Binary cross entropy loss function """
    dE = torch_nn.BCELoss()
    return dE

############### Trainning ################################################################################

def trainingLoop(gnn, batchSize, nepochs, opti, lossFunc):
    Ntot = tensors.jet_true_E.shape[0]

    # opti is a function returning a pytorch optimizer
    optimizer = opti(gnn.parameters())

    for epoch in range(nepochs):
        for i,b in enumerate(range(0,Ntot, batchSize)):
            stop = min(Ntot, b+batchSize)
            #print("epoch",epoch, "step", i)

            # Create a batch :
            feat_b, edg_b, nnod_b, nedg_b = batch(b,stop)

            # Model prediction
            modelPrediction = gnn(feat_b, edg_b, nnod_b, nedg_b)

            # Retrieve the true label for this batch
            trueLabel = tensors.jet_true_E[b:stop]

            # Calculate the loss
            l = lossFunc(modelPrediction, feat_b[:,0:1], nnod_b, trueLabel)
            l = l.mean()

            # Perform an optimization step
            optimizer.zero_grad()
            l.backward()
            optimizer.step()

            if i%100:
                print( 'loss ',l)

    return gnn

## Run the training (`partial` is a trick to predefine which parameters are going to be passed to the optimizer)
#trainingLoop(gnn, 1024, 3, partial(torch.optim.NAdam, lr=0.0001) , jetRMSE)

# the training might not work with the above. The point is the input data set must be filtered to avoid
# noisy or unphysical true jets. A minimal sanitizing trick can be :
#                  tensors.jet_true_E = torch.clip(tensors.jet_true_E,500) #


#def calibrateJet(gnn):
#    Ntot = tensors.jet_nconstit.shape[0]
#    
#    # we don't need to calculate gradients, just to obtain the predictions
#    # so run under no_grad() for better perf
#    with torch.no_grad():
#        feat_b,edg_b,nnod_b,nedg_b = batch(0,Ntot)
#        calibFactors = gnn(feat_b,edg_b,nnod_b,nedg_b)
#        calibFactors = calibFactors.reshape(-1)
#    jet_calib_E = resumJetE(tensors.cst_E*calibFactors, tensors.jet_nconstit)
#
#    return jet_calib_E
#
#
## ************************
## Does it work ??
#
#import matplotlib
#import matplotlib.pyplot as plt
#plt.ion()
#
#eR = tensors.jet_E/tensors.jet_true_E
#plt.hist( eR, bins=100, range=(0,2), label='no calib' )
#
#eR_calib = calibrateJet(gnn)/tensors.jet_true_E
#plt.hist( eR_calib, bins=100, range=(0,2) , label='gnn calib')





